#include <stdio.h>

long sum_divisors(long number)
{
    long total=0;
    int i;
    for(i=1; i<=number; i++){
        if((number%i)==0)
            total+=i;
    }
    return total;
}


// Brute force; check the summ for each growing integer and stop at the first
int main()
{
    long input = 33100000;
//    long input = 150;
    long result = 0;
    long i=0;
    printf("Starting...\n");
    
    while(result<input)
    {
        i++;
        result = sum_divisors(i)*10;
//        printf("%ld :\t%ld\n", i, result);        
    }    
    printf("Day 20, solution #2= %ld :\t(%ld presents!)\n", i, result);
}

