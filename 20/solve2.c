#include <stdio.h>

#define INPUT (33100000)
//#define INPUT (150)
#define N_HOUSES (INPUT/11)


long houses[N_HOUSES];


// Memory intensive brute force; add to each house and then find the minimum
int main()
{
    long i, j;
    for(i=1; i<N_HOUSES; i++)
    {
        long count=0;
        for(j=i; j<N_HOUSES && count<50; j+=i, count++)
        {
            houses[j]+=i*11;
        }
    }
    for(i=0; i<N_HOUSES; i++)
    {
//        printf("%ld :\t%ld\n", i, houses[i]);
        if(houses[i]>= INPUT)
        {
            printf("Day 20, solution #2= %ld :\t(%ld presents!)\n", i, houses[i]);
            break;                    
        }
    }    
}
