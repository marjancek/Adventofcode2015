

transforms = []
original = ""
trans = {}

with open("input", "r") as f:
    for line in f:
        line = line.strip(' \t\n\r')
        t = line.split(' => ')
        if(len(t)==2):
            transforms.append( (t[0], t[1] ) )
        else:
            original = line


#for k in transforms:
#    print(k[0] + "<->" + k[1])
                
l = len(original)
for i in range(1, l):
#    print("-------------------\n" + str(i) )
    for k in transforms:
        if original[i:].startswith(k[0]):
            trans[original[:i] + k[1] + original[i+len(k[0]):]]=True
#            print(original[:i] + k[1] + original[i+len(k[0]):])

print "Day 19th result #1 = " + str(len(trans))



