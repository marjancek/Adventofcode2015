
transforms = []
original = ""

todo = []
reductions = {}

best = float('inf')       

with open("input", "r") as f:
    for line in f:
        line = line.strip(' \t\n\r')
        t = line.split(' => ')
        if(len(t)==2):
            transforms.append( (t[0], t[1] ) )
        else:
            original = line

todo.append( (original,0) )


# Use stack recursion: put any new reduced molecule on a stack for later
# further reduction, keeping always the 'cheapest' cost for such
while(len(todo)>0):
    t = todo.pop()
    
    chain = t[0]
    
#    if(chain=="e"):
#        print ("Found solution with "+ str(t[1]) + " steps")

    # already known, an not cheaper than thought? no new information
    if( (chain in reductions) and reductions[chain] <= t[1] ):
        break
    # New information! even if we knew the molecule, we didn't kow it could
    # be produced this cheap; we need to look into any further reduction
    reductions[chain] = t[1]
    l = len(chain)
    for i in range(0, l):       # check all potential starting points
        for k in transforms:    # and all potential reductions
            if chain[i:].startswith(k[1]):
#                print(chain[i:] + "==" + k[1] + "\t("+str(i)+")")
                newchain = chain[:i] + k[0] + chain[i+len(k[1]):]
                if( not (newchain in reductions)):
                    todo.append( (newchain,t[1]+1) )
                elif( reductions[newchain] > t[1]+1 ): # found a better solution!
                    reductions[newchain] = t[1]+1
                    todo.append( (newchain, t[1]+1) )

print "Day 19th result #2 = " + str(reductions["e"])
