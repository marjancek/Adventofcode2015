import random

sizes = []

with open("input", "r") as f:
    for line in f:
        line = line.strip(' \t\n\r')
        sizes.append(int(line))

print sizes

print sum(sizes)
sizes = sizes[::-1]
#random.shuffle(sizes)
each = sum(sizes)//4
avgsize = len(sizes)//4
print each
print avgsize

global best
best = (999,999999)

def score(used):
    pieces = 0
    weight = 0
    qe = 1
    for w in used:
        pieces = pieces+1
        weight = weight + w
        qe=qe*w
        if(weight==each):
#            print (pieces, qe)
            return (pieces, qe)
    return (999,999999)
    
    
def solve(used, unused, length):
    global best 
#    print used,  unused
    if(len(unused)<1 or len(used)>length):
        return (999,999999)
    if(sum(used) < each):
#        print sum(used)
        for i in range(len(unused)):
            un = unused[:]
            del un[i]
            sc =  solve(used + [unused[i]], un, length)
#            print used + [unused[i]], un
#            print sc
            #print best
            if( not (sc is None) and (sc[0]<best[0] or ( sc[0]==best[0] and sc[1]<best[1]) ) ):
                print "Best"
                print sc
                best = sc
    elif(sum(used) == each):
#        print "Equal:"
#<        print score(used)
        return score(used)
    else:
        return (999,999999)


for i in range(avgsize):
    result = solve([], sizes, i)
    print i
    print result
    print best
    print "-----------------"
    if( not (best is None) and best[0]<999):
        break
        
print best
