using System;
using System.Text;
using System.Security.Cryptography;

class MainClass {
    
public static string CalculateMD5Hash(string input)
{
    // step 1, calculate MD5 hash from input
    MD5 md5 = System.Security.Cryptography.MD5.Create();
    byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
    byte[] hash = md5.ComputeHash(inputBytes);
 
    // step 2, convert byte array to hex string
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < hash.Length; i++)
    {
        sb.Append(hash[i].ToString("X2"));
    }
    return sb.ToString();
}


  public static void Main (string[] args) {
     
     string key = "abcdef";
     
    for (int i = 0; i < int.MaxValue; i++)
    {
        string hash =  CalculateMD5Hash(key+i);
        if(String.Compare(hash.Substring(0,6), "000000")==0){
            Console.WriteLine("Found AdventCoint!\t\t" + i);
            return;
        }
    }
    Console.WriteLine ("No Advent coin found for secret " + key);
  }
}
      
