Weapons:    Cost  Damage  Armor
Dagger        8     4       0
Shortsword   10     5       0
Warhammer    25     6       0
Longsword    40     7       0
Greataxe     74     8       0

Armor:      Cost  Damage  Armor
Leather      13     0       1
Chainmail    31     0       2
Splintmail   53     0       3
Bandedmail   75     0       4
Platemail   102     0       5

Rings:      Cost  Damage  Armor
Damage +1    25     1       0
Damage +2    50     2       0
Damage +3   100     3       0
Defense +1   20     0       1
Defense +2   40     0       2
Defense +3   80     0       3



Hit Points: 109
Damage: 8
Armor: 2


We need to tame morefrom him than he takes from us; that is:
The Damage we inflict minus its armor must be greater than
the Damage hte Boss it inflicts minus our armor
(considering also that no attack takes less than 1 point)


Damage  4 & Armor 7  (4-2>8-7)

    Damage:
        8
    Armor:
        102+40      142
        75+20+40    135
        75+80       155
        53+20+80    153
        53+40+80    173
        31+40+80    151
    min: 150

Damage  5 & Armor 6  (5-2>8-6)
    Damage:
        8+25
        10
    Armor:
        102+20      122
        75+40       115
        53+20+40    113
    min:
        123


Damage  6 & Armor 5  (6-2>8-5)

    Damage:
        25
    Armor:
        75+20       95
        53+40       93
        31+20+40    91
    min:
        116
        

Damage  7 & Armor 4  (7-2>8-4)

    Damage:
        40
    Armor:
        75          75
        53+20       73
        31+40       71
        13+20+40    73
    min:
        111     <-------------------------------------
        
Damage  8 & Armor 3  (8-2>8-3)

    Damage:
        74      74
        40+25   65
    Armor:
        53      53
        31+20   51
        13+40   53
    min:
        118

Damage  9 & Armor 2  (9-2>8-2)
    Damage
        74+25       99
        40+50       90
        25+100     125
    Armor:
        31          31
        13+20       33     
    min:
        121

