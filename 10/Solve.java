class Main {
  
  public static int runlength(String s, int i){
      char c = s.charAt(i);
      int k;
      for(k=i; k<s.length() && c==s.charAt(k);k++ );
      
      return k-i;
  }
  
  public static String lookNsay(String input)
  {
      StringBuffer b=new StringBuffer();
      int i=0;
      while(i<input.length())
      {
         int r=runlength(input, i);
         b.append(""+r);
         b.append(""+input.charAt(i));
         i+=r;
      }
      return b.toString();
  }
  
  public static void main(String[] args) {
    String input = "3113322113";

    //System.out.println(lookNsay("111221"));
    
    for(int i = 0; i<50; i++){
        input = lookNsay(input);
        if(i==39)
            System.out.println("Day 10, problem #1 = " + input.length());
    }

    System.out.println("Day 10, problem #2 = " + input.length());

  } 
}
