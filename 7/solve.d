// Tested with the GNU D compiler gdc
import std.stdio, std.string, std.conv;

auto NOT = "NOT";
auto LSHIFT = "LSHIFT";
auto RSHIFT ="RSHIFT";
auto AND = "AND";
auto OR = "OR";
auto NIL = "";
auto DEB = false;

int main()
{
    Node[string] cables;  // dictionary of Nodes "z = [x OP y | OP x | x]"
    foreach (line; "input".File.byLine){
        auto params = split(line);
        if(DEB) line.writeln;
        string k = to!string(params[$-1]);
        cables[k] = new Node(params);
    }
    writeln("----------------------------\nStart solving for 'a'...");
    ushort res = cables["a"].solve(cables);       // Find the value for node 'a'
    writefln("Day 7 solution #1 = %d.", res);
    
    // Part 2; reset all cables and force 'b' to the value of 'a' 
    foreach (key; cables.keys)
        cables[key].reset();
    cables["b"].force(res);
    
    res = cables["a"].solve(cables);              // Find the value for node 'a'
    writefln("Day 7 solution #2 = %d.", res);
    writeln("----------------------------");
    return 0;
}

class Node {
 string operation;
 string operand1;
 string operand2;
 ushort value;
 bool solved;   // wesupose a static end value  => loops will be ignored.

    // constructor; save the operator and operands, or NIL when not applicable
    this(char[][] params) {             
        if(cmp(params[0], NOT)==0){
            operation = NOT;
            operand1 = to!string(params[1]);
            operand2 = NIL;
        }else if(params.length<4){
            operation = NIL;
            operand1 = to!string(params[0]);
            operand2 = NIL;
        }else{
            operation = to!string(params[1]);
            operand1 = to!string(params[0]);
            operand2 = to!string(params[2]);
        }
        solved=false;   // not solved yet!
    }
    
    void reset(){
        solved = false;
    }
    
    void force(ushort _value){
        value = _value;
        solved = true;
    }
    
    // convert to numeric if a value; ask node to solve itself otherwise.
    ushort resolve(string operand, Node[string] cables){
        if( isNumeric(operand) && operand[0] >= '0' && operand[0]<='9' )
            return to!ushort(operand);
        return cables[operand].solve(cables);
    }
    
    // If not solved, fetch the parameters and perform the operation
    ushort solve(Node[string] cables) {
        if(solved)
            return value;
       // writefln("Parameters are %(%s %).", [operation, operand1, operand2]);
        if(DEB) writef("Solve %s(%s, %s)\n", operation, operand1, operand2);
        ushort a, b;
        switch(operation){
            case "NOT": 
                a = resolve(operand1, cables);
                value = ~a;
                if(DEB) writef("\tResolved NOT %s -> NOT %d = %d\n", operand1, a, value);
                break;
            case "AND":
                a = resolve(operand1, cables);
                b = resolve(operand2, cables);
                value = a&b; 
                if(DEB) writef("\tResolved %s AND %s -> %d & %d = %d\n", operand1, operand2, a, b, value);
                break; 
            case "OR":
                a = resolve(operand1, cables);
                b = resolve(operand2, cables);
                value = a|b;
                if(DEB) writef("\tResolved %s OR %s -> %d | %d = %d\n", operand1, operand2, a, b, value);
                break;
            case "RSHIFT":
                a = resolve(operand1, cables);
                b = resolve(operand2, cables);
                value = a>>b;
                if(DEB) writef("\tResolved %s RSHIFT %s -> %d >> %d = %d\n", operand1, operand2, a, b, value);
                break;
            case "LSHIFT":
                a = resolve(operand1, cables);
                b = resolve(operand2, cables);
                value = to!ushort(a<<b);
                if(DEB) writef("\tResolved %s LSHIFT %s -> %d << %d = %d\n", operand1, operand2, a, b, value);
                break;
            default:
                value = resolve(operand1, cables);
                if(DEB) writef("\tResolved %s as plain number %d\n", operand1, value);
        }
        solved = true;
        return value;
    }    
}
