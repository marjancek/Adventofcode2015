var fs = require("fs");
var dict = {};

function compute(x, y){
  return [x[0]+y[0], x[1]+y[1]];
}

function move(b){
    switch(b){
        case '<': return [-1,  0];
        case '^': return [ 0,  1];
        case 'v': return [ 0, -1];
        case '>': return [ 1,  0];
        default : return [ 0,  0];
    }
}

function include(t){
    if(t in dict){
        dict[t] = dict[t] + 1;}
    else
        dict[t]=1;
}
  
fs.readFile('input', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }

  var s=[0,0]
  var r=[0,0]
  include(s);
  include(r);
  SantasTurn=true;
  
  for(var i = 0; i < data.length;i++){
    if(SantasTurn)
    {
        s = compute(s, move(data[i]) )
        include(s);
        SantasTurn=false;
    }else{
        r = compute(r, move(data[i]) )
        include(r);
        SantasTurn=true;
    }
  }

    console.log("Solution to Day 3 #2:");
    console.log(Object.keys(dict).length);

});
