#!/usr/bin/perl
use strict;
use warnings;
 
my $filename = "input";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";

my $count = 0;  
 
while (my $row = <$fh>) {
  chomp $row;
#  print "$row\n";
  if($row =~ m/\w*(\w\w)\w*\1\w*/){      # matching ...CC...CC...
      if($row =~ m/^\w*(\w)\w\1\w*$/){   # matching ...CxC...
         $count++;
      }
  }
  
}
print "$count\n";


