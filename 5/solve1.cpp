// Compile with "g++ solve1.cpp"
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

class Word{
  public: 
    Word(std::string s): value(s){};
    
    size_t count_vowels(){
        size_t count=0;    // could hav done a vector and a loop, right?
        count += std::count(value.begin(), value.end(), 'a');
        count += std::count(value.begin(), value.end(), 'i');
        count += std::count(value.begin(), value.end(), 'u');
        count += std::count(value.begin(), value.end(), 'e');
        count += std::count(value.begin(), value.end(), 'o');
        
        return count;
    }
    
    bool has_double(){
        for(std::string::size_type i = 1; i < value.size(); ++i) {
            if(value[i]==value[i-1])
                return true;
        }
        return false;
    }
    
    bool has_forbiden(){
        if( value.find("ab") !=std::string::npos )
            return true;
        if( value.find("cd") !=std::string::npos )
            return true;
        if( value.find("pq") !=std::string::npos )
            return true;
        if( value.find("xy") !=std::string::npos )
            return true;
        return false;
    }

    bool is_good(){
        return !has_forbiden() && has_double() && count_vowels()>=3;
    }
    
  private:
    std::string value = NULL;
};


int main(){
    std::string line;
    std::ifstream infile("input");

    int good_ones=0;
    while (std::getline(infile, line))
    {
        Word w(line);
        if(w.is_good())
            good_ones++;
    }
    std::cout << "Day 5 #1 :" << good_ones << "\n";
}
