import numpy
import itertools

def hapiness(m, p):
    fr = p[0]
    total = 0
    for to in p:
        total += m[fr, to]
        total += m[to, fr]
        fr = to
    total += m[fr, p[0]]
    total += m[p[0], fr]
 
    return total
    
lines = []
people = {}
index = 0;
indexes = []
with open("input", "r") as f:
    for line in f:
        lines.append(line)
        t = line.split(' ')
        if( not (t[0] in people) ):
            people[t[0]]=index;
            index+=1
        if( not (t[10].rstrip('.\n') in people) ):
            people[t[10].rstrip('.\n')]=index;
            index+=1

people["you"] = index
index=index+1

m = numpy.zeros((index, index))
for line in lines:
    t = line.split(' ')
    val = int(t[3])
    if(t[2]=="lose"):
        val = -val
    m[people[t[0]], people[t[10].rstrip('.\n')] ] = val

l = range(0,index)
perm = itertools.permutations(l)
i = 0

happiest = 0
for p in perm:
    i+=1
    total=hapiness(m, p)
    if(happiest<total):
        happiest=total

print("Day 13 problem #2 = " + str(happiest))
