

function coor = coords (token)
    coor = strsplit(token, ",");
    coor = [  str2num(coor{1}), str2num(coor{2})  ];
endfunction

fh = fopen ("input");

m = zeros(1000, 1000);
line = fgetl (fh);

do
    tokens = strsplit(line, " ");
    if( strcmp( tokens{1}, "toggle") )
        coor1 = coords( tokens{2} );
        coor2 = coords( tokens{4} );
        sizes = [coor2(1) - coor1(1)+1, coor2(2) - coor1(2)+1];
        m( coor1(1):coor2(1), coor1(2):coor2(2) ) +=2 ;
    else
        coor1 = coords( tokens{3} );
        coor2 = coords( tokens{5} );
        sizes = [coor2(1) - coor1(1)+1, coor2(2) - coor1(2)+1];

        if( strcmp( tokens{2}, "on") )
            m( coor1(1):coor2(1), coor1(2):coor2(2) ) += 1;
        else
            m( coor1(1):coor2(1), coor1(2):coor2(2) ) -=  1;        
        endif
    endif
    m(m<0) = 0; # went below zero? fix that.
    line = fgetl (fh);
until (line==-1);

fclose (fh);

disp("Day 6, solution #2:"), disp(sum(sum(m)));

x=1:50;
x = x/50;
c1 = [x/3;x;x/2];
c1 = transpose(c1);
figure (1);
imshow(m, c1)

x=1:50;
x = x/50;
x = log(x);
x = x-min(x);
x = x/max(x);
c2 = [x/3;x;x/2];
c2 = transpose(c2);
figure (2);
imshow(m, c2)

x=1:50;
x = x/50;
x = exp(x);
x = x-min(x);
x = x/max(x);
c3 = [x/3;x;x/2];
c3 = transpose(c3);
figure (3);
imshow(m, c3)

z = zeros(size(c1)(1), 1);

figure (4);
c4 = [ c2(:,2), c3(:,2), z];
imshow(m, c4)

figure (5);
c4 = [ c1(:,2), c3(:,2), z];
imshow(m, c4)

figure (6);
c4 = [ z, c1(:,2), c2(:,2)];
imshow(transpose(m), c4)

figure (7);
c4 = [ c3(:,2), c1(:,2), z];
imshow(m, c4)

k=waitforbuttonpress;
