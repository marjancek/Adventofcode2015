

program = []
registers = {'a':1 , 'b':0}
with open("input", "r") as f:
    for line in f:
        line = line.strip(' \t\n\r')
        program.append(line)


pc = 0
while(pc<len(program)):
    ins = program[pc][0:3]
#    print("pc:"+str(pc)+"\t"+program[pc]+"\t\t"+str(registers))
    if(ins == 'jmp'):
        pc = pc + int(program[pc][3:])
        continue
    elif(ins == 'jie'):
        if(registers[program[pc][4]] % 2 == 0):
            pc = pc + int(program[pc][7:])
            continue
    elif(ins == 'jio'):
        if(registers[program[pc][4]] == 1):
            pc = pc + int(program[pc][7:])
            continue
    elif(ins == 'hlf'):
        registers[program[pc][4]] = registers[program[pc][4]] >> 1
    elif(ins == 'tpl'):
        registers[program[pc][4]] = registers[program[pc][4]] * 3
    elif(ins == 'inc'):
        registers[program[pc][4]] = registers[program[pc][4]] +1
    else:
        break;
    pc=pc+1
    
print "Register b contains: " + str(registers['b'])
