#include <stdio.h>
#include <string.h>

struct Character{
    int hit;
};

struct Player{
    struct Character character;
    int mana;
    int mana_used;
};


struct Boss{
    struct Character character;
    int damage:8;
};

struct Game{
    int poisons;
    int recharges;
    int shields;
    int player_plays;
    struct Player player;
    struct Boss boss;
};

int character_do_damage(struct Character *c, int damage)
{
    c->hit-=damage;
    return c->hit;
}

int character_get_hit(struct Character *c)
{
    return c->hit;
}

struct Character *character_new(struct Character *c, int hit_value){
    c->hit = hit_value;
    return c; 
}

int player_get_mana(struct Player *p)
{
    return p->mana;
}

int player_get_mana_used(struct Player *p)
{
    return p->mana_used;
}

int player_add_mana(struct Player *p, int new_mana)
{
    p->mana+=new_mana;
    return p->mana;
}

int player_use_mana(struct Player *p, int mana_used)
{
    struct Character *c = (void*) p;
    p->mana-=mana_used;
    p->mana_used+=mana_used;
    if(p->mana<=0)
        c->hit=0;
    return c->hit;
}

int player_heal(struct Player *p, int healing)
{
    struct Character *c = (void*) p;
    c->hit+=healing;
    return c->hit;
}

struct Player *player_new(struct Player *p, int hit, int mana_value)
{
    struct Character *c = (void*) p;
    character_new(c, hit);
    p->mana=mana_value;
    p->mana_used=0;
}

int boss_get_damage(struct Boss *b)
{
    return b->damage;
}

struct Boss *boss_new(struct Boss *b, int hit, int damage_value)
{
    struct Character *c = (void*) b;
    character_new(c, hit);
    b->damage=damage_value;
}

struct Game game_new(struct Game *g)
{
//    player_new(&g->player, 10, 250);
//    boss_new(&g->boss, 14, 8);  // RSDPM
    player_new(&g->player, 50, 500);
    boss_new(&g->boss, 55, 8);
    g->poisons=0;
    g->recharges=0;
    g->shields=0;
    g->player_plays=1;
}

int game_character_play(struct Game *g){
    if(g->recharges){
        g->recharges--;
        player_add_mana(&g->player, 101);
    }
    if(g->poisons){
        g->poisons--;
        character_do_damage( (struct Character *) (void*) &g->boss, 3);
//        boss_damage(&g->boss, 3);        
    }
    if(g->shields){
        g->shields--;
    }
    return !game_finished(g);

}


int game_player_play(struct Game *g, char action){
    game_character_play(g);
    g->player_plays=0;

    if(game_finished(g) ) return 0;

    switch(action){
        int hit;
        case 'M': 
            if(player_use_mana(&g->player, 53)<=0)   return -2;
            if( character_do_damage( (struct Character *) (void*) &g->boss, 4)<=0 )
                return -1;
            break;
        case 'D': 
            if(player_heal(&g->player, 2)<=0)        return -1;
            if(player_use_mana(&g->player, 73)<=0)   return -1;
            if( character_do_damage( (struct Character *) (void*) &g->boss, 2)<=0 )
                return -1;
            break;
        case 'S': 
            if(g->shields>1){
                character_do_damage( (struct Character *) (void*) &g->player, 
                    character_get_hit( (struct Character *) (void*) &g->player)  );
                return -2;
            }
            if(player_use_mana(&g->player, 113)<=0)   return -3;
            g->shields=6;
            break;
        case 'P': 
            if(g->poisons){
                character_do_damage( (struct Character *) (void*) &g->player, 
                    character_get_hit( (struct Character *) (void*) &g->player)  );
                return -2;
            }
            if(player_use_mana(&g->player, 173)<=0)   return -4;
            g->poisons=6;
            break;
        case 'R': 
            if(g->recharges){
                character_do_damage( (struct Character *) (void*) &g->player, 
                    character_get_hit( (struct Character *) (void*) &g->player)  );
                return -5;
             }
//                printf(" %d ", g->recharges);
            if(player_use_mana(&g->player, 229)<=0)   return -1;
            g->recharges=5;
            break;
    }
    return 0;
}

int game_boss_play(struct Game *g){
    game_character_play(g);
    g->player_plays=1;

    if(game_finished(g) ) return -1;

    int d = boss_get_damage(&g->boss);
    if(g->shields>0)
        d-=7;
    d = d>=1 ? d : 1;
    character_do_damage( (struct Character *) (void*) &g->player, d);

    return character_get_hit(  ( (struct Character *) (void*) &g->player )   );
}

int player_print_status(struct Player *p)
{
    struct Character *c = (void*) p;
    printf("PLAYER: hits:(%d)\tmana(%d)\n", 
            character_get_hit(c), player_get_mana(p) );
}

int boss_print_status(struct Boss *b)
{
    struct Character *c = (void*) b;
    printf("BOSS  : hits:(%d)\tdamage(%d)\n", 
        character_get_hit(c), boss_get_damage(b) );
}

int game_print_satus(struct Game *g)
{
    printf("\n----------------------------\n");
    if(g->player_plays)
    {
        player_print_status(&g->player);
        boss_print_status(&g->boss);
    }
    else
    {
        boss_print_status(&g->boss);
        player_print_status(&g->player);
    }
    printf("poisons = %d\n", g->poisons);
    printf("recharges = %d\n", g->recharges);
    printf("shields = %d\n", g->shields);
}
int game_finished(struct Game* g){
    return  character_get_hit(  ( (struct Character *) (void*) &g->player) ) <= 0 ||
            character_get_hit(  ( (struct Character *) (void*) &g->boss )  ) <= 0;

}

int game_player_won(struct Game* g){
    return  character_get_hit(  ( (struct Character *) (void*) &g->player) ) >0 &&
            character_get_hit(  ( (struct Character *) (void*) &g->boss )  ) <= 0;
}


int try_move(struct Game game, char move, int best, char* s){ 

//    int i=0;
//    while(i++<depth)
//        printf(" ");
//    printf("%c\n", move);
//    game_print_satus(&game);
    game_player_play(&game, move);
    //if( game_player_play(&game, move)<0)
      //  return -1;

//    game_print_satus(&game);
    if( game_finished(&game) )
    {
//        game_print_satus(&game);
        if( game_player_won(&game) )
        {
            printf("Won with %d manas:\n%s\n", player_get_mana_used(&game.player), s);
            return player_get_mana_used(&game.player);
            }
        else
            return -1;
    }
    if( best < player_get_mana_used(&game.player) )
        return -1;
    game_boss_play(&game);
//    game_print_satus(&game);
    if( game_finished(&game) )
    {
 //       game_print_satus(&game);
        if( game_player_won(&game) )
        {
            printf("Won with %d manas:\n%s\n", player_get_mana_used(&game.player), s);
            return player_get_mana_used(&game.player);
        }
        else
            return -1;
    }
//    game_print_satus(&game);
    return find_lowest_mana_solution(game, best, s);
}

int find_lowest_mana_solution(struct Game game, int best, char* s){

    int l=strlen(s);
    char moves[] = "MDSPR";
    int i;
    for(i=0;i<5;i++)
    {
        s[l] = moves[i];
        int mana=try_move(game, moves[i], best, s);
//        printf("we're on mana: %d\n", mana);        
//        game_print_satus(&game);

        if(mana>0 && mana<best){
            best=mana; 
//            printf("we're on mana: %d\n(%s)\n", mana,s);
        }
    }
    s[l] = '\0';
    return best;
}


int main(){
    char s[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    struct Game game;
    game_new(&game);
    game_print_satus(&game);
    int mana = find_lowest_mana_solution(game, 2147483645, s);
    printf("Solution: %d\n", mana);
}

int tmain(){
    struct Game game;
    game_new(&game);
//    game_print_satus(&game);
    printf("It's alive!");
    char s[] = "RPSMPMMMM";
    //char s[] = "PRRPSSPPM"; 
//    char s[] = "SRPSRPMMMMMM";
    int i=0;
    while(s[i] && !game_finished(&game))
    {
        printf("\n-->%c<--\n",s[i]);
        game_player_play(&game, s[i]);
        game_print_satus(&game);
        game_boss_play(&game);
        game_print_satus(&game);
        i++;
    }
    
    if(game_player_won(&game) )
        printf("\nCongratulations Player 1!!\n");
    else
        printf("\nBoad job Player 1...\n");
        
    printf("Using %d mana poits\n", player_get_mana_used(&game.player));
}


// 1408 too high
// 1295 too high
// 914 too low
