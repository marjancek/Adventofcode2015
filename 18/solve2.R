# Read the data as string lines
#input <- read.csv('input2', header = FALSE, sep = "", quote = "")
input <- read.csv('input', header = FALSE, sep = "", quote = "")
input<-lapply(input, as.character)   # convert from Factor to Character
input<-lapply(input, strsplit, '')  # Split into characters
input<-data.frame(input, stringsAsFactors=FALSE)    #re-frame into matrix
input<-t(input)=='#'       # transform into logical states

#dim(input)
#sum(input)
#summary(input)
image(t(-input))
input[1,1]<-TRUE
input[1, ncol(input)]<-TRUE
input[nrow(input),1]<-TRUE
input[nrow(input), ncol(input)]<-TRUE

for (i in 1:100) {
    su<-rbind(input[-1,],0)             # values shifted up one possition
    sd<-rbind(0,input[-nrow(input),])   # values shifted down
    n<- su+sd+ cbind(input[,-1],0) + cbind(0,input[,-ncol(input)])+ # 1up, 1down, 1left, 1right
        cbind(su[,-1],0) + cbind(0,su[,-ncol(su)]) + cbind(sd[,-1],0) + cbind(0,sd[,-ncol(sd)]) # 4 diagonals
    # let's make some animations!
    png(filename=paste("./advent18b", formatC(i, width=3, format="d", flag="0"), "png", sep ="."  )  )
    image(-t(input))
    dev.off()
    # we'll also animate the heatmap
    png(filename=paste("./advent18-b", formatC(i, width=3, format="d", flag="0"), "png", sep ="."  )  )
    image(-t(n))
    dev.off()

    current<-which(input)       # indexes of lights currently on
    input[current]<- n[current]==2 | n[current]==3      # turn on those with 2 or 3 neighbours, otherwise off
    input[-current] <- n[-current]==3                   # turn on those with 3 neighbours, otherwise turn off
    input[1,1]<-TRUE
    input[1, ncol(input)]<-TRUE
    input[nrow(input),1]<-TRUE
    input[nrow(input), ncol(input)]<-TRUE

    print(i)
    #    sum(input)
}
sum(input)
