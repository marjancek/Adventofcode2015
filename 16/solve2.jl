# Show only the lines that don't go against the given rules
# values in look up mus match exactly (recycled from solution #1)
# values in gttm must be greater than the reference values
# and values in fttm bust me fewer than that of the reference values
# at the end, we hope we will get only one line

gttm = [
(7, "cats:"),
(3, "trees:")
]

fttm = [
(3, "pomeranians:"),
(5, "goldfish:")
]

lookup = [(
"children: 3", "children:"),
("samoyeds: 2", "samoyeds:"),
("akitas: 0", "akitas:"),
("vizslas: 0", "vizslas:"),
("cars: 2", "cars:"),
("perfumes: 1", "perfumes:")]

open("input","r") do f
    for line in eachline(f)
    line = replace(line, ", ", " ")
    
        good=true
        
        for t in lookup
            if(  searchindex(line, t[2])>0 && searchindex(line, t[1])==0 )
                good=false
                break
            end
        end
        
        for t in gttm
            ndx=searchindex(line, t[2])
            if(  ndx>0 )
                # get the int value at the end of the "whatevs :" string
                val = int(line[ndx+length(t[2]):ndx+length(t[2])+2])
                if(t[1]>=val)
                    good=false
                    break
                end
            end
        end

        for t in fttm
            ndx=searchindex(line, t[2])
            if(  ndx>0 )
                # get the int value at the end of the "whatevs :" string
                val = int(line[ndx+length(t[2]):ndx+length(t[2])+2])
                if(t[1]<=val)
                    good=false
                    break
                end
            end
        end

        # if still "good", show it.
        if good
            println("good line: ", line)
        end
    end
end
