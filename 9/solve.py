import numpy
import itertools

def path_length(m, p):
    fr = p[0]
    km = 0
    for to in p:
        km += m[fr, to]
        fr = to 
    return km
    
lines = []
cities = {}
index = 0;
indexes = []
with open("input", "r") as f:
    for line in f:
        lines.append(line)
        t = line.split(' ')
        if( not (t[0] in cities) ):
            cities[t[0]]=index;
            index+=1
        if( not (t[2] in cities) ):
            cities[t[2]]=index;
            index+=1
#print index

m = numpy.zeros((index, index))
for line in lines:
    t = line.split(' ')
    m[cities[t[0]], cities[t[2]] ] = int(t[4])
    m[cities[t[2]], cities[t[0]] ] = int(t[4])
    
l = range(0,index)
perm = itertools.permutations(l)
i = 0
shortest = 9999999999999
longest = 0
for p in perm:
    i+=1
    km=path_length(m, p)
    if(shortest>km):
        shortest=km
    if(longest<km):
        longest=km
#print(str(i)+" different permutations.")
#print(cities)
#print(m)    
print("Day 9 problem #1 = " + str(shortest))
print("Day 9 problem #2 = " + str(longest))



