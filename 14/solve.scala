import scala.io.Source

val race_length = 2503

class Deer(name: String, speed: Int, endurance: Int, rest: Int) {
   val s: Int = speed
   val e: Int = endurance
   val r: Int = rest
   val n: String = name
   var p: Int = 0
   
   def posAt(seconds: Int) :Int = {
      var cycle = e+r;
      var cycles = (seconds/cycle).floor.toInt
      var reminder = seconds - (cycles*cycle)
      if( reminder > e)
        reminder = e

      return cycles*e*s + reminder*s
   }
   
   def getName() :String ={
    return n
   }
   
   def addPoint() ={
    p+=1
   }
   
   def getPoints() :Int = {
    return p
   }
}


var deers = Array[Deer]()
for(line <- Source.fromFile("input").getLines())
{
    var values = line.split(" ") 
    var deer = new Deer(values(0), values(3).toInt, values(6).toInt, values(13).toInt)
    deers = deers :+ deer

} 

var best = 0
for(deer <- deers)
{
    var distance = deer.posAt(race_length)
    if(best<distance)
        best=distance
}


println("Day 14 problem #1:" + best)


for (seconds <- 1 until race_length)
{
    var most_km=0
    var best_deer : Deer=null
    
    for(deer <- deers)
    {
        var distance = deer.posAt(seconds)
        if(most_km<distance)
        {
            most_km=distance
            best_deer=deer
        }
    }
    best_deer.addPoint()
//    println(best_deer.getName + ":" +best_deer.getPoints() )
}

best = 0
for(deer <- deers)
{
    if(best<deer.getPoints())
        best=deer.getPoints()
}

println("Day 14 problem #2:" + best)

